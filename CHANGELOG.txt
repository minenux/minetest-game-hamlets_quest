

	Hamlet's Quest - Changelog
	==========================


	v. 3.2.2 (2020-10-21)
	Changed:
	- Explosives re-enabled due to DFCaverns' dependency.


	v. 3.2.1 (2020-07-28)
	Changed:
	- "Crafting Guide" replaced with "Crafting Guide Plus" https://github.com/random-geek/cg_plus
	

	v. 3.2.0 (2020-07-27)
	Added:
	- "Autoplant" https://github.com/krondor-game/autoplant


	v. 3.1.4 (2020-07-27)
	Changed:
	- "HUD Bars" updated to v2.3.2
	- "Basic Materials" updated to v2020-07-12
	- "Round Tree Trunks" updated to v1.1.0
	- "Skylayer" updated to v2020-05-03
	- "Stonebrick Dungeons" updated to v0.4.1
	- "Radiant Damage" updated to v2020-06-04
	- "Soft Leaves" updated to v0.2.1
	- "Farming Redo" updated to v2020-07-11
	- "Crafting Guide" updated to v2020-07-27
	- "Recycle Age" updated to v1.3.4
	- "Hidden Doors" updated to v1.12.1
	- "Smaller Steps" updated to v1.4.1


	v. 3.1.3 (2020-07-25)
	Removed:
	- "Item Drop"


	v. 3.1.2 (2020-07-13)
	- Fixes for hq_tweaks (thank you to Goats)


	v. 3.1.1 (2020-07-10)
	Changed:
	- Game base updated to Minetest Game v5.3.0


	v. 3.1.0 (2020-06-19)
	Added:
	- Option to toggle the Crafting Guide's progressive mode via Advanced settings; "On" by default - "Off" = display all recipes.
	- Option to select the preferred display style for the HUD bars, via Advanced settings.
	- Option to set the preferred order to sort the HUD bars, via Advanced settings.
	- "Farming Redo" https://forum.minetest.net/viewtopic.php?t=9019
	- "Item Drop" https://forum.minetest.net/viewtopic.php?t=16913
	- Option to set the "Item Drop" pick up sound, via Advanced settings.
	- "North Compass" https://github.com/FaceDeer/north_compass
	- "Round Tree Trunks" https://forum.minetest.net/viewtopic.php?t=20036

	Changed:
	- Armor's bar will be hidden when the player isn't wearing any armor, this can be changed via Advanced settings.
	- "Weather" (MTG mods) turned off to prevent conflicts with other weather-related mods (minetest.conf at line 4).
	- "3D armor" updated to version 2020-05-14.
	- "Castle Gates" updated to version 2020-05-07.
	- "Crafting Guide" updated to version 2020-06-11.
	- "Hardcore Torchs" updated to version 2020-06-13.
	- "Hunger NG" updated to version 2020-05-12.
	- "Moon Phases" updated to v2.1.0.
	- "Ropes and rope ladders" updated to version 2020-06-01.
	- "HB Sprint" updated to version 2020-04-15.
	- "Unifieddyes" updated to version 2020-06-03.
	- "Unified Hammers" updated to version 2020-04-11.

	Removed:
	- Minetest Game's "farming" mod (switched to Farming Redo).
	- "Crops" mod (switched to Farming Redo).


	v. 3.0.0 (2020-04-19)
	Added:
	- "Enchanting" https://forum.minetest.net/viewtopic.php?t=14087

	Changed:
	- Game's version changed to 3.x due to non-backward compatibility.
	
	Removed:
	- Mobs.


	v. 2.6.0 RC2 (2020-04-13)
	Added:
	- "Basic Materials" https://forum.minetest.net/viewtopic.php?t=21000
	- "Moon Phase" https://forum.minetest.net/viewtopic.php?t=24536
	- "Sky Layer" https://forum.minetest.net/viewtopic.php?t=15733

	Changed:
	- Game's base updated to Minetest Game v5.2.0.
	- All the textures have been optimized with optipng.
	- All the mods have been updated to their latest release.
	
	Removed:
	- PKArcs made of cobble or mossycobble due to a bug that crashes the game; existing nodes in-world will be swapped to their stone-made counterparts.


	v. 2.6.0-dev
	Added:
	- "Bunkbed" https://forum.minetest.net/viewtopic.php?t=23684
	- "Campfire" https://forum.minetest.net/viewtopic.php?t=10569
	- "Credits"
	- "Gravelsieve" https://forum.minetest.net/viewtopic.php?t=17893
	- "Hunger NG" https://forum.minetest.net/viewtopic.php?t=19664
	- "Placeable ingots" https://forum.minetest.net/viewtopic.php?t=21491
	- "Radiant damage" https://forum.minetest.net/viewtopic.php?t=20185
	- "Re-Cycle Age" https://forum.minetest.net/viewtopic.php?t=23414
	- "Soft Leaves" https://forum.minetest.net/viewtopic.php?t=20141
	- "Unified Hammers" https://forum.minetest.net/viewtopic.php?t=22720
	
	Changed:
	- License changed to EUPL v1.2 or later.
	- Game's base updated to Minetest Game v5.1.0.
	- Log level set to "Warning", was "Error".
	- Game's textures (screenshot.png; ../menu/*.png) have been optimized with optipng.
	- The Crafting Guide is now "inventory-only", and in "progressive-mode".
	- Heat sources (fire and lava) will deal damage ("Radiant Damage").
	
	Removed:
	- "Loot" (already present in Minetest Game's mods).
	- "Intllib" and "MTG_i18n" (switched to Minetest Engine's translator).
	- "HBHunger" (switched to "Hunger NG").
	- "Diet" (conflicting with "Hunger NG").
	- "Toolranks" (switched to "Re-Cycle Age").
	- "Killer Nodes" (outdated and laggy).
	- "PKArcs Darkage", moved into "HQ Tweaks".
	- Any mobs' related mod, they have been placed into a different branch.


	v. 2.5.2 (2019-09-28)

	- Changed:
		Mobs Redo: v1.5.0
		Snow Walkers: v0.2.0


	v. 2.5.1 (2019-09-16)

	- Added: description.txt

	- Changed:
		"Craftguide": development version
		"Smaller steps": v1.3.0

	- Removed:
		Minetest Game's "dungeon_loot"

	- Notes:
		Fixed possible craftguide crash on *NIX O.S. (See: )
		All the stair nodes now use proper shapes.
		Dungeons' loot is already managed by .../hamlets_quest/mods/map/loot/


	v. 2.5.0 (2019-09-14)

	- Added "Binoculars" (modified version, i.e. "telescope")
	- Added "Simple Arcs" by TumeniNodes
	- Removed "Simple Arcs" by PEAK
	- Changed:
		"craftguide": v1.8
		"doc": v1.3.0
		"doc_minetest_game": development version
		"hudbars": v2.1.0


	v. 2.4.2 (2019-09-12)

	- Updated.


	v. 2.4.1 (2018-09-23)

	- Removed doubled Mobs Redo folder.


	v. 2.4.0 (2018-09-22)

	- Added "Castle Doors", "Cottage Doors" and "Door Wood" by Don.
	- Reverted mobs' difficulty to 1 (was 2)
		introduced in v. 1.1.2-dev (2017-09-18)
	- Mobs' difficulty now can be changed using the advanced settings menu.
	- Deactivated frames' registration for inexistent items (e.g. tnt)
	- All the modules have been updated to their latest versions.


	v. 2.3.0 (2018-06-03)

	- Updated the game base to Minetest Game v0.4.17
	- Added: Path marker signs [breadcrumbs], Frame, Mountain climbing [handholds], Hardcore torchs [hardtorch], Sounding line, Snow Walkers [mobs_others]
	- Removed: Real Torches
	- All the modules have been updated to their latest versions.


	v. 2.2.2 (2018-05-16)

	- Updated 3D Armor, Everamzah Backpacks, Mobs Redo, Mobs Animal,
		Real Torch, Unified Dyes


	v. 2.2.1 (2018-05-05)

	- Updated Mobs Redo
	- Fixed the goblins' spawning


	v. 2.2.0 (2018-05-03)

	- Added SFInv Buttons by Wuzzy, check the inventory's "More" tab.
   - Cavern spawn: Settings/Advanced Settings/Games/Hamlet's Quest
   - Added Under Sky by Shara Redcat.
	- Added charcoal block.
	- Real Minerals' ingots and Default's ingots now can be swapped.
	- Real Minerals' metal blocks now can be converted into ingots.
	- Day lenght set to one hour.
	- Real Torches set to last for 30min max.
   - 20 hydration pts now last for 30min.
   - Steel canteen and bronze canteen's capacity: 20, 30 (was 40, 60).
   - Leaves set to phantom, climbable.
   - Stone arrows' recipe modified to use rocks or flints, output = 3.
   - Some DFCaverns' flora nodes now glow.
   - DFCaverns' "bloodthorns" now hurt.
   - Cut the number of spawned goblins.
   - Swords' damage increased of 2pts.
   - Crocodiles' spawn restricted to river water.
	- Added Fort Spikes.
	- Added Extra armors for 3d_armor (v0.2)


	v. 2.1.0 (2018-03-02)

	- Disabled cacti and wooden made armors and shields. They were active by mistake.
	- Improved chains and chandeliers' look.
	- Added charcoal, crafted by cooking tree nodes.
	- Added in-game documentation, i.e. encyclopedia and node inspector tool.
	- Added Real Trees' nodes to Fallen Trees.
	- Added Hard Trees Redo, that disables digging tree nodes by bare hands; disables wooden pick and axe; adds rocks to dirt nodes; adds sticks to leaves nodes; changes stone tools recipes: you need rocks instead of stone, you can also use flints. Rocks' textures are ugly.
	- Cacti now do damage; floor torches have been tweaked to damage only if you are standing very close to them or right over them.
	- Dungeons' chests now use the default Minetest Game's chests.
	- Dungeons are now made of brighter bricks, the old ones were way too dark.
	- Balrog mob added; very hard to see one, almost impossible to defeat.
	- Wooden bucket readded, it was removed by mistake.
	- Lava buckets removed for realism's sake.
	- Almost all of the game tweaks have been moved to hq_tweaks.
	- Night skip while sleeping has been re-enabled for realism's sake.
	- TNT module added to allow Dungeon Masters fireballs' explosions.
	- Coal powder - to light torches - now can be crafted by using charcoal also.
	- ../bash_tools/ files have been updated as needed.
	- All the modules have been updated to their latest versions.


	v. 2.0.0 (2018-01-18)

	- Switched to Minetest Game v0.4.16 as base
	- Switched to R-0ne's mobs_water module
	- Added library module by v-rob
	- Added Simple Arcs module by PEAK extended it to support Darkage's nodes, thanks to Napiophelios
	- Added Arctic Life module by NathanSalapat
	- Added Desert Life module by NathanSalapat
	- Added All Walls module by v-rob
	- Added ../bash_tools/clean_and_rename.sh
	- Removed nsspf_remover module
	- All the modules have been updated to their latest versions


	v. 1.2.3-dev (2017-10-07)

	- Minetest Game's modules updated (0.4.17-dev 2017-10-03)
	- All the other modules have been updated


	v. 1.2.2-dev (2017-09-30)

	- Fixed the issue with the sharks stuck on ground
	- Reduced the ground turtle's size


	v. 1.2.1-dev (2017-09-28)

	- Added TS Furniture module.


	v. 1.2.1-dev (2017-09-27)

	- Removed duplicated mtg_mods folder


	v. 1.2.0-dev (2017-09-27)

	- Updated Minetest Game's modules to 0.4.17-dev (2017-09-21)
	- Updated all the other modules to their latest versions
	- Modified settingtypes.txt to allow configuring the Hidden Doors module via Settings/Advanced Settings/Games/Hamlet's Quest
	- Added Castle Gates module
	- Added Mob Horse module
	- Updated ../bash_tools/download.sh and update.sh
	- Updated ../documents/credits.txt


	v. 1.1.1 (2017-09-19)
	---------------------

	- Added anvil's French and Italian locales.
	- Modified loot to use Castle Storage's crates.


	v. 1.1.0 (2017-09-19)
	---------------------

	- NSSPF Remover disabled by default, activable via Advanced Settings.
	- Castle Tapestries added.
	- Added a third main menu's background image.
	- Other minor tweaks.
	- Added ../documents/credits.txt
	- Added ../bash_tools/download.sh
	- Added ../bash_tools/update.sh


	v. 1.1.2-dev (2017-09-18)
	-------------------------

	- Updated Minetest Game's modules to 0.4.17-dev (2017-09-17)
	- All the other modules have been updated to their latest versions.
	- Mobs' difficulty set to "2", it was "1".
	- Mobs out of viewing range are now disabled.
	- HUDBars' order rearranged.
	- Added a new main menu's background image.
	- Main menu's background images resized and compressed via pngcrush.
	- Fixed errors in README.txt.


	v. 1.1.1-dev (2017-09-16)
	-------------------------

	- Disabled Mobs Goblins' glowing cobblestone.
	- Added Castle Storage support to Loot.
	- Other minor tweaks and fixes.


	v. 1.1.0-dev (2017-09-15)
	-------------------------

	- Added ../documents/changelog.txt.
	- Edited README.txt to match Minetest Game's versioning.
	- Disabled Thirsty's tier 4+ components.
	- Added Stonebrick Dungeons v. 0.1.0.
	- Added NSSPF remover v. 0.1.0.
	- Added Loot
