### HAMLET'S QUEST
![Hamlet's Quest's screenshot](screenshot.png)  
**_A survival, medieval themed game._**  

**Version:** 3.2.2  
**Source code's license:** [EUPL v1.2][1] or later.  
**Media (Textures, Sounds) license:** [CC BY-SA 4.0 International][2] or later.  
**Except where differently stated, check each subfolder.**

__Advanced settings:__  
Settings -> All settings -> Games -> hamlets_quest


### Installation

Unzip the archive, rename the folder to hamlets_quest and place it in  
../minetest/games/

GNU+Linux: If you use a system-wide installation place it in  
~/.minetest/games/

For further information or help see:  
https://wiki.minetest.net/Games#Installing_games


[1]: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863
[2]: https://creativecommons.org/licenses/by-sa/4.0/
