--[[
	Smaller Steps - Makes stairs and slabs use smaller shapes.
	Copyright © 2018-2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Procedures
--

-- Nodes overrider
-- "argument string node shape" can be 'normal', 'outer', 'inner' or 'slab'.
smaller_steps.pr_NodeOverrider = function(a_s_node_name, a_s_node_shape)

	-- Constants
	local t_SHAPE_NORMAL = {
		drawtype = "nodebox",
		node_box = {
			type = "fixed",
			fixed = {
				{-0.5, -0.5, -0.5, 0.5, -0.166667, 0.5},
				{-0.5, -0.166667, -0.166667, 0.5, 0.166667, 0.5},
				{-0.5, 0.166667, 0.166667, 0.5, 0.5, 0.5},
			},
		},
		selection_box = {
			type = "fixed",
			fixed = {
				{-0.5, -0.5, -0.5, 0.5, -0.166667, 0.5},
				{-0.5, -0.166667, -0.166667, 0.5, 0.166667, 0.5},
				{-0.5, 0.166667, 0.166667, 0.5, 0.5, 0.5},
			},
		},
		collision_box = {
			type = "fixed",
			fixed = {
				{-0.5, -0.5, -0.5, 0.5, -0.166667, 0.5},
				{-0.5, -0.166667, -0.166667, 0.5, 0.166667, 0.5},
				{-0.5, 0.166667, 0.166667, 0.5, 0.5, 0.5},
			},
		},
	}

	local t_SHAPE_OUTER = {
		drawtype = "nodebox",
		node_box = {
			type = "fixed",
			fixed = {
				{-0.5, -0.5, -0.5, 0.5, -0.166667, 0.5},
				{-0.5, -0.166667, -0.166667, 0.166667, 0.166667, 0.5},
				{-0.5, 0.166667, 0.166667, -0.166667, 0.5, 0.5},
			},
		},
		selection_box = {
			type = "fixed",
			fixed = {
				{-0.5, -0.5, -0.5, 0.5, -0.166667, 0.5},
				{-0.5, -0.166667, -0.166667, 0.166667, 0.166667, 0.5},
				{-0.5, 0.166667, 0.166667, -0.166667, 0.5, 0.5},
			},
		},
		collision_box = {
			type = "fixed",
			fixed = {
				{-0.5, -0.5, -0.5, 0.5, -0.166667, 0.5},
				{-0.5, -0.166667, -0.166667, 0.166667, 0.166667, 0.5},
				{-0.5, 0.166667, 0.166667, -0.166667, 0.5, 0.5},
			},
		},
	}

	local t_SHAPE_INNER = {
		drawtype = "nodebox",
		node_box = {
			type = "fixed",
			fixed = {
				{-0.5, -0.5, -0.5, 0.5, -0.166667, 0.5},
				{-0.5, -0.166667, -0.5, 0.166667, 0.166667, 0.5},
				{-0.5, 0.166667, -0.5, -0.166667, 0.5, 0.5},
				{0.166667, -0.166667, -0.166667, 0.5, 0.166667, 0.5},
				{-0.166667, 0.166667, 0.166667, 0.5, 0.5, 0.5},
			},
		},
		selection_box = {
			type = "fixed",
			fixed = {
				{-0.5, -0.5, -0.5, 0.5, -0.166667, 0.5},
				{-0.5, -0.166667, -0.5, 0.166667, 0.166667, 0.5},
				{-0.5, 0.166667, -0.5, -0.166667, 0.5, 0.5},
				{0.166667, -0.166667, -0.166667, 0.5, 0.166667, 0.5},
				{-0.166667, 0.166667, 0.166667, 0.5, 0.5, 0.5},
			},
		},
		collision_box = {
			type = "fixed",
			fixed = {
				{-0.5, -0.5, -0.5, 0.5, -0.166667, 0.5},
				{-0.5, -0.166667, -0.5, 0.166667, 0.166667, 0.5},
				{-0.5, 0.166667, -0.5, -0.166667, 0.5, 0.5},
				{0.166667, -0.166667, -0.166667, 0.5, 0.166667, 0.5},
				{-0.166667, 0.166667, 0.166667, 0.5, 0.5, 0.5},
			},
		},
	}

	local t_SHAPE_SLAB = {
		drawtype = "nodebox",
		node_box = {
			type = "fixed",
			fixed = {
				{-0.5, -0.5, -0.5, 0.5, -0.166667, 0.5},
			},
		},
		selection_box = {
			type = "fixed",
			fixed = {
				{-0.5, -0.5, -0.5, 0.5, -0.166667, 0.5},
			},
		},
		collision_box = {
			type = "fixed",
			fixed = {
				{-0.5, -0.5, -0.5, 0.5, -0.166667, 0.5},
			},
		},
	}

	if (a_s_node_shape == 'normal') then
		minetest.override_item(a_s_node_name, t_SHAPE_NORMAL)

	elseif (a_s_node_shape == 'outer') then
		minetest.override_item(a_s_node_name, t_SHAPE_OUTER)

	elseif (a_s_node_shape == 'inner') then
		minetest.override_item(a_s_node_name, t_SHAPE_INNER)

	elseif (a_s_node_shape == 'slab') then
		minetest.override_item(a_s_node_name, t_SHAPE_SLAB)

	end
end

-- Minetest logger
smaller_steps.pr_LogMessage = function()

	-- Constants
	local s_LOG_LEVEL = minetest.settings:get('debug_log_level')
	local s_LOG_MESSAGE = '[Mod] Smaller Steps [v1.4.1] loaded.'

	-- Body
	if (s_LOG_LEVEL == nil)
	or (s_LOG_LEVEL == 'action')
	or (s_LOG_LEVEL == 'info')
	or (s_LOG_LEVEL == 'verbose')
	then
		minetest.log('action', s_LOG_MESSAGE)
	end
end
