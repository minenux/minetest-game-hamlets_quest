### SMALLER STEPS
![Smaller Steps' screenshot](screenshot.png)  
**_Makes stairs and slabs use smaller shapes._**

**Version:** 1.4.1  
**License:** [EUPL v1.2][1] or later.

**Dependencies:** none  
**Supported:** castle_masonry, darkage (Addi's fork), df_trees, df_underworld_items, farming (either MTG's or TenPlus1's), my_door_wood, stairs

### Installation

Unzip the archive, rename the folder to smaller_steps and place it in  
../minetest/mods/

If you only want this to be used in a single world, place it in  
../minetest/worlds/WORLD_NAME/worldmods/

GNU+Linux - If you use a system-wide installation place it in  
~/.minetest/mods/

For further information or help see:  
https://wiki.minetest.net/Help:Installing_Mods


[1]: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863
