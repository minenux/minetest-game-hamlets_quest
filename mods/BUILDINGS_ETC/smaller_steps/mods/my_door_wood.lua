--[[
	Smaller Steps - Makes stairs and slabs use smaller shapes.
	Copyright © 2018-2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Procedure
--

local pr_StairsOverriders = function()

	-- Constants
	local t_nodesStairsNormal = {
		'stairs:stair_wood_brown', 'stairs:stair_wood_dark_grey',
		'stairs:stair_wood_grey', 'stairs:stair_wood_red',
		'stairs:stair_wood_white', 'stairs:stair_wood_yellow',
		'stairs:stair_wood_black'
	}

	local t_nodesStairsOuter = {
		'stairs:stair_outer_wood_brown', 'stairs:stair_outer_wood_dark_grey',
		'stairs:stair_outer_wood_grey', 'stairs:stair_outer_wood_red',
		'stairs:stair_outer_wood_white', 'stairs:stair_outer_wood_yellow',
		'stairs:stair_outer_wood_black'
	}

	local t_nodesStairsInner = {
		'stairs:stair_inner_wood_brown', 'stairs:stair_inner_wood_dark_grey',
		'stairs:stair_inner_wood_grey', 'stairs:stair_inner_wood_red',
		'stairs:stair_inner_wood_white', 'stairs:stair_inner_wood_yellow',
		'stairs:stair_inner_wood_black'
	}

	local t_nodesSlabs = {
		'stairs:slab_wood_brown', 'stairs:slab_wood_dark_grey',
		'stairs:slab_wood_grey', 'stairs:slab_wood_red',
		'stairs:slab_wood_white', 'stairs:slab_wood_yellow',
		'stairs:slab_wood_black'
	}

	for i_element = 1, #t_nodesStairsNormal do
		smaller_steps.pr_NodeOverrider(t_nodesStairsNormal[i_element], 'normal')
	end

	for i_element = 1, #t_nodesStairsOuter do
		smaller_steps.pr_NodeOverrider(t_nodesStairsOuter[i_element], 'outer')
	end

	for i_element = 1, #t_nodesStairsInner do
		smaller_steps.pr_NodeOverrider(t_nodesStairsInner[i_element], 'inner')
	end

	for i_element = 1, #t_nodesSlabs do
		smaller_steps.pr_NodeOverrider(t_nodesSlabs[i_element], 'slab')
	end
end


--
-- Main body
--

pr_StairsOverriders()
