--[[
	Smaller Steps - Makes stairs and slabs use smaller shapes.
	Copyright © 2018-2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Procedure
--

local pr_StairsOverriders = function()

	-- Constants
	local t_nodesStairsNormal = {
		'stairs:stair_darkage_basalt', 'stairs:stair_darkage_basalt_brick',
		'stairs:stair_darkage_basalt_rubble',
		'stairs:stair_darkage_chalked_bricks',
		'stairs:stair_darkage_gneiss', 'stairs:stair_darkage_gneiss_brick',
		'stairs:stair_darkage_gneiss_rubble', 'stairs:stair_darkage_marble',
		'stairs:stair_darkage_marble_tile',
		'stairs:stair_darkage_old_tuff_bricks',
		'stairs:stair_darkage_ors', 'stairs:stair_darkage_ors_brick',
		'stairs:stair_darkage_ors_rubble',
		'stairs:stair_darkage_rhyolitic_tuff',
		'stairs:stair_darkage_rhyolitic_tuff_bricks',
		'stairs:stair_darkage_schist',
		'stairs:stair_darkage_serpentine', 'stairs:stair_darkage_shale',
		'stairs:stair_darkage_slate', 'stairs:stair_darkage_slate_brick',
		'stairs:stair_darkage_slate_rubble', 'stairs:stair_darkage_slate_tile',
		'stairs:stair_darkage_stone_brick', 'stairs:stair_darkage_straw_bale',
		'stairs:stair_darkage_tuff', 'stairs:stair_darkage_tuff_bricks'
	}

	local t_nodesStairsOuter = {
		'stairs:stair_outer_darkage_basalt',
		'stairs:stair_outer_darkage_basalt_brick',
		'stairs:stair_outer_darkage_basalt_rubble',
		'stairs:stair_outer_darkage_chalked_bricks',
		'stairs:stair_outer_darkage_gneiss',
		'stairs:stair_outer_darkage_gneiss_brick',
		'stairs:stair_outer_darkage_gneiss_rubble',
		'stairs:stair_outer_darkage_marble',
		'stairs:stair_outer_darkage_marble_tile',
		'stairs:stair_outer_darkage_old_tuff_bricks',
		'stairs:stair_outer_darkage_ors',
		'stairs:stair_outer_darkage_ors_brick',
		'stairs:stair_outer_darkage_ors_rubble',
		'stairs:stair_outer_darkage_rhyolitic_tuff',
		'stairs:stair_outer_darkage_rhyolitic_tuff_bricks',
		'stairs:stair_outer_darkage_schist',
		'stairs:stair_outer_darkage_serpentine',
		'stairs:stair_outer_darkage_shale',
		'stairs:stair_outer_darkage_slate',
		'stairs:stair_outer_darkage_slate_brick',
		'stairs:stair_outer_darkage_slate_rubble',
		'stairs:stair_outer_darkage_slate_tile',
		'stairs:stair_outer_darkage_stone_brick',
		'stairs:stair_outer_darkage_straw_bale',
		'stairs:stair_outer_darkage_tuff',
		'stairs:stair_outer_darkage_tuff_bricks'
	}

	local t_nodesStairsInner = {
		'stairs:stair_inner_darkage_basalt',
		'stairs:stair_inner_darkage_basalt_brick',
		'stairs:stair_inner_darkage_basalt_rubble',
		'stairs:stair_inner_darkage_chalked_bricks',
		'stairs:stair_inner_darkage_gneiss',
		'stairs:stair_inner_darkage_gneiss_brick',
		'stairs:stair_inner_darkage_gneiss_rubble',
		'stairs:stair_inner_darkage_marble',
		'stairs:stair_inner_darkage_marble_tile',
		'stairs:stair_inner_darkage_old_tuff_bricks',
		'stairs:stair_inner_darkage_ors',
		'stairs:stair_inner_darkage_ors_brick',
		'stairs:stair_inner_darkage_ors_rubble',
		'stairs:stair_inner_darkage_rhyolitic_tuff',
		'stairs:stair_inner_darkage_rhyolitic_tuff_bricks',
		'stairs:stair_inner_darkage_schist',
		'stairs:stair_inner_darkage_serpentine',
		'stairs:stair_inner_darkage_shale',
		'stairs:stair_inner_darkage_slate',
		'stairs:stair_inner_darkage_slate_brick',
		'stairs:stair_inner_darkage_slate_rubble',
		'stairs:stair_inner_darkage_slate_tile',
		'stairs:stair_inner_darkage_stone_brick',
		'stairs:stair_inner_darkage_straw_bale',
		'stairs:stair_inner_darkage_tuff',
		'stairs:stair_inner_darkage_tuff_bricks'
	}

	local t_nodesSlabs = {
		'stairs:slab_darkage_basalt', 'stairs:slab_darkage_basalt_brick',
		'stairs:slab_darkage_basalt_rubble',
		'stairs:slab_darkage_chalked_bricks',
		'stairs:slab_darkage_gneiss', 'stairs:slab_darkage_gneiss_brick',
		'stairs:slab_darkage_gneiss_rubble', 'stairs:slab_darkage_marble',
		'stairs:slab_darkage_marble_tile',
		'stairs:slab_darkage_old_tuff_bricks',
		'stairs:slab_darkage_ors', 'stairs:slab_darkage_ors_brick',
		'stairs:slab_darkage_ors_rubble', 'stairs:slab_darkage_rhyolitic_tuff',
		'stairs:slab_darkage_rhyolitic_tuff_bricks',
		'stairs:slab_darkage_schist',
		'stairs:slab_darkage_serpentine', 'stairs:slab_darkage_shale',
		'stairs:slab_darkage_slate', 'stairs:slab_darkage_slate_brick',
		'stairs:slab_darkage_slate_rubble', 'stairs:slab_darkage_slate_tile',
		'stairs:slab_darkage_stone_brick', 'stairs:slab_darkage_straw_bale',
		'stairs:slab_darkage_tuff', 'stairs:slab_darkage_tuff_bricks'
	}

	for i_element = 1, #t_nodesStairsNormal do
		smaller_steps.pr_NodeOverrider(t_nodesStairsNormal[i_element], 'normal')
	end

	for i_element = 1, #t_nodesStairsOuter do
		smaller_steps.pr_NodeOverrider(t_nodesStairsOuter[i_element], 'outer')
	end

	for i_element = 1, #t_nodesStairsInner do
		smaller_steps.pr_NodeOverrider(t_nodesStairsInner[i_element], 'inner')
	end

	for i_element = 1, #t_nodesSlabs do
		smaller_steps.pr_NodeOverrider(t_nodesSlabs[i_element], 'slab')
	end
end


--
-- Main body
--

pr_StairsOverriders()
