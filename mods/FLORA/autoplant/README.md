Autoplant [![](https://img.shields.io/badge/license-WTFPL-green.svg?style=flat-square)](http://www.wtfpl.net/)
=========

Self-planting saplings for [minetest](http://minetest.net/).
