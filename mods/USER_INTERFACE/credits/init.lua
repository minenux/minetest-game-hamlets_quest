--[[

	Credits - Adds a credits tab to the player's inventory.
	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Used for localization

local S = minetest.get_translator('credits')


--
-- Procedure
--

-- Minetest logger
local pr_LogMessage = function()

	-- Constants
	local s_LOG_LEVEL = minetest.settings:get('debug_log_level')
	local s_LOG_MESSAGE = '[Mod] Credits [v0.1.2] loaded.'

	-- Body
	if (s_LOG_LEVEL == nil)
	or (s_LOG_LEVEL == 'action')
	or (s_LOG_LEVEL == 'info')
	or (s_LOG_LEVEL == 'verbose')
	then
		minetest.log('action', s_LOG_MESSAGE)
	end
end



--
-- Function
--

sfinv.register_page('credits:credits', {
	title = S('Credits'),
	get = function(self, player, context)
		local s_CREDITS = ',Hamlet\'s Quest v3.2.1 \"Valhalleluja\",============================,,BASED ON,============================,Minetest Game,By Various Contributors,Version: 5.3.0,License: LGPL v2.1,,USER INTERFACE,============================,Crafting Guide Plus,Version: 2019-07-03,By: random_geek,License: LGPL v3,,Credits,Version: 0.1.1,By: Hamlet,License: EUPL v1.2 or later,,Help,Version: 1.3.0,By: Wuzzy,License: MIT,,Minetest Game item help,Version: 2018-10-14,By: Wuzzy,License: MIT,,HUD bars,Version: 2.3.2,By: Wuzzy,License: MIT,,Simple Fast Inventory Buttons,Version: 1.0.0,By: Wuzzy,License: MIT,,MAP RELATED,============================,Basic Materials,Version: 2020-07-12,By: VanessaE,License: LGPL v3.0,,Charcoal Lump,Version: 2019-11-21,By: torusJKL,License: GPL v2.0 or later,,Dwarf Fortress style caverns,Version: Modpack v2.0,By: FaceDeer,License: MIT,,Moon Phases,Version: 2.1.0,By: TestificateMods,License: LGPL v3.0,,Real Minerals,Version: 2017-09-02,By: FaceDeer,License: GPL v3.0,,Round Tree Trunks,Version: 1.1.0,By: Hamlet,License: License: EUPL v1.2 or later,,Skylayer,Version: 2020-05-03,By: xeranas,License: MIT,,Snow rain clouds (with sound),Version: 2020-05-22,By: paramat,License: MIT,,Stonebrick Dungeons,Version: 0.4.1,By: Hamlet,License: EUPL v1.2 or later,,Under Sky,Version: 2017-07-23,By: Shara,License: MIT,,Unified Dyes,Version: 2020-06-03,By: VanessaE,License: GPL v2.0,,PHYSICS AND DAMAGE,============================,Fallen Nodes,Version: 1.5.0,By: Hamlet,License: EUPL v1.2 or later,,Fallen Trees,Version: 1.3.0,By: Hamlet,License: EUPL v1.2 or later,,Hard Trees Redo,Version: 0.2.0,By: Hamlet,License: EUPL v1.2 or later,,Radiant Damage,Version: 2020-06-04,By: FaceDeer,License: MIT,,Soft Leaves,Version: 0.2.1,By: Hamlet,License: EUPL v1.2 or later,,FLORA,============================,Autoplant,Version: 2015-11-28,By: neoascetic,License: WTFPL,,Desert Life,Version: 2020-01-05,By: Nathan.S,License: CC BY-SA v4.0,,Farming Redo,Version: 2020-07-11,By: TenPlus1,License: MIT,,Real Trees,Version: 2017-01-11,By: yzelast,License: WTFPL,,CHARACTER,============================,3D Armor,Version: 2020-05-14,By: stu,License: LGPL v2.1,,Armor HUD bar,Version: 1.0.0,By: Wuzzy,License: MIT,,Sprint w. hudbars hunger monoids support,Version: 2020-04-15,By: texmex,License: LGPL v3.0,,Hunger NG,Version: 2020-05-12,By: Linuxdirk,License: GPL v3.0,,Thirsty,Version: 0.10.2,By: Ben,License: LGPL v2.1,,Extra armors for 3d_armor,Version: 0.2,By: davidthecreator,License: CC BY-SA v3.0,,TOOLS AND ALIKE,============================,Anvil,Version: 2020-03-19,By: Sokomine,License: GPL v3.0,,Archer,Version: 2017-09-04,By: Saavedra29,License: WTFPL,,Backpacks,Version: 2019-01-13,By: everamzah,License: GPL v3.0 or later,,Castle Weapons,Version: 2020-02-29,By: philipbenr & Dan DunCombe & FaceDeer,License: MIT,,Enchanting,Version: 2019-12-25bis,By: jp & Hamlet,License: GPL v3.0,,Fort Spikes,Version: 2017-08-15,By: xeranas,License: MIT,,Gravel Sieve,Version: 2018-03-11,By: joe7575,License: LGPL v2.1 or later,,Hardcore Torchs,Version: 2020-06-13,By: BrunoMine,License: LGPL v3.0,,Mountain Climbing,Version: 2018-06-21,By: Shara,License: MIT,,North Compass,Version: 2020-01-14,By: FaceDeer,License: MIT,,Path marker signs,Version: 2020-02-19,By: FaceDeer,License: MIT,,Placeable Books,Version: 2017-06-04,By: everamzah,License: LGPL v2.1 or later,,Re-Cycle Age,Version: v1.3.4,By: Hamlet,License: EUPL v1.2 or later,,Ropes and rope ladders,Version: 2020-06-01,By: FaceDeer,License: MIT,,Sounding Line,Version: 2020-03-03,By: FaceDeer,License: MIT,,Unified Hammers,Version: 2020-04-11,By: Linuxdirk,License: GPL v3.0,,Wooden Bucket,Version: 2019-08-01,By: duane,License: LGPL v2.0,,BUILDINGS AND SIMILAR,============================,Bunkbed,Version: 2019-11-22,By: Nordall,License: LGPL v2.1 or later,,Campfire Updated,Version: 2019-11-29,By: Napiophelios,License: GPL v3.0,,Walls All,Version: 2017-07-25,By: v-rob,License: WTFPL,,Carpet,Version: 2012-08-26,By: Jordach,License: LGPL,,My Castle Doors,Version: 2018-06-13,By: Don,License: DWYWPL,,My Cottage Doors,Version: 2018-06-13,By: Don,License: DWYWPL,,My Door Wood,Version: 2018-06-13,By: Don,License: DWYWPL,,Castle Gates,Version: 2020-05-07,By: philipbenr & Dan DunCombe & FaceDeer,License: MIT,,Castle Lighting,Version: 2020-02-28,By: philipbenr & Dan DunCombe & FaceDeer,License: MIT,,Castle Masonry,Version: 2020-02-18,By: philipbenr & Dan DunCombe & FaceDeer,License: MIT,,Castle Shields,Version: 2019-11-23,By: philipbenr & Dan DunCombe & FaceDeer,License: MIT,,Castle Storage,Version: 2020-02-28,By: philipbenr & Dan DunCombe & FaceDeer,License: MIT,,Castle Tapestries,Version: 2020-02-16,By: philipbenr & Dan DunCombe & FaceDeer,License: MIT,,Darkage,Version: 2020-01-03,By: MasterGollum & addi,License: WTFPL,,Frame,Version: 2019-10-27,By: sofar,License: LGPL v2.1 or later,,Hidden Doors,Version: v1.12.1,By: Hamlet & Napiophelios,License: EUPL v1.2 or later,,Library,Version: 2019-11-13,By: v-rob & Hamlet,License: MIT,,Placeable Ingots,Version: 2019-09-10,By: Skamiz Kazzarch,License: LGPL v1.2,,PKArcs - with doors,Version: 2019-09-08,By: PEAK & TumeniNodes,License: LGPL v2.1,,Smaller Steps,Version: v1.4.1,By: Hamlet,License: GPL v3.0,,Furniture,Version: 2020-03-07,By: Thomas-S,License: MIT,'

		return sfinv.make_formspec(player, context,
				'textlist[0,0;7.8,9.2;;' .. s_CREDITS .. ']', false)
	end
})


pr_LogMessage()
