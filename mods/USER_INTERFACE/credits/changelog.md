# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/).


## [Unreleased]

	- No further feature planned.



## [0.1.1] - 2020-04-17
### Changed

	- Switched to a single string to avoid "chunk has too many syntax levels" error.



## [0.1.0] - 2020-04-09
### Added

	- Initial release.
