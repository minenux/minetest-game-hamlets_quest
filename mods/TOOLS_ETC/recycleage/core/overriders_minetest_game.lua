--[[

	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Function
--

-- Used to determine which scrap material should be used.
local fn_DefineScraps = function(a_s_itemstring)
	local s_material = ''

	if (string.match(a_s_itemstring, 'stone') ~= nil) then
		s_material = 'default:cobblestone'

	elseif (string.match(a_s_itemstring, 'bronze') ~= nil) then
		s_material = 'recycleage:scrap_metal_bronze'

	elseif (string.match(a_s_itemstring, 'steel') ~= nil) then
		s_material = 'recycleage:scrap_metal_steel'

	elseif (string.match(a_s_itemstring, 'mese') ~= nil) then
		s_material = 'recycleage:scrap_shards_mese'

	elseif (string.match(a_s_itemstring, 'diamond') ~= nil) then
		s_material = 'recycleage:scrap_shards_diamond'

	end

	return s_material
end


--
-- Procedure
--

-- Used to override the default Minetest Game's tools and weapons.
local pr_MTGoverriders = function()

	-- Local constants

	-- Default drop amounts per tool's type
	local i_dropPick = minetest.settings:get('recycleage_drop_pick') or 2
	local i_dropShovel = minetest.settings:get('recycleage_drop_shovel') or 0
	local i_dropAxe = minetest.settings:get('recycleage_drop_axe') or 2
	local i_dropSword = minetest.settings:get('recycleage_drop_sword') or 1
	local i_dropHoe = minetest.settings:get('recycleage_drop_hoe') or 1

	local t_MTGtools = {

		-- Wooden
		{'default:pick_wood',		i_dropPick},
		{'default:shovel_wood',		i_dropShovel},
		{'default:axe_wood',		i_dropAxe},
		{'farming:hoe_wood',		i_dropHoe},

		-- Stone
		{'default:pick_stone',		i_dropPick},
		{'default:shovel_stone',	i_dropShovel},
		{'default:axe_stone',		i_dropAxe},
		{'farming:hoe_stone',		i_dropHoe},

		-- Bronze
		{'default:pick_bronze',		i_dropPick},
		{'default:shovel_bronze',	i_dropShovel},
		{'default:axe_bronze',		i_dropAxe},
		{'farming:hoe_bronze',		i_dropHoe},

		-- Steel
		{'default:pick_steel',		i_dropPick},
		{'default:shovel_steel',	i_dropShovel},
		{'default:axe_steel',		i_dropAxe},
		{'farming:hoe_steel',		i_dropHoe},

		-- Mese
		{'default:pick_mese',		i_dropPick},
		{'default:shovel_mese',		i_dropShovel},
		{'default:axe_mese',		i_dropAxe},
		{'farming:hoe_mese',		i_dropHoe},

		-- Diamond
		{'default:pick_diamond',	i_dropPick},
		{'default:shovel_diamond',	i_dropShovel},
		{'default:axe_diamond',		i_dropAxe},
		{'farming:hoe_diamond',		i_dropHoe}
	}

	-- Weapons
	local t_MTGswords = {
		{'default:sword_wood',		i_dropSword},
		{'default:sword_stone',		i_dropSword},
		{'default:sword_bronze',	i_dropSword},
		{'default:sword_steel',		i_dropSword},
		{'default:sword_mese',		i_dropSword},
		{'default:sword_diamond',	i_dropSword}
	}


	--
	-- Body
	--

	-- Tools' overrider
	for _, value in pairs(t_MTGtools) do
		local s_tool = value[1]
		local i_headPieces = value[2]
		local s_handle = 'default:stick'
		local s_handlePieces = 1
		local s_scraps = fn_DefineScraps(s_tool)

		recycleage.after_use_overrider(s_tool, s_handle, s_handlePieces,
			s_scraps, i_headPieces)
	end

	-- Weapons' overrider
	for _, value in pairs(t_MTGswords) do
		local s_tool = value[1]
		local i_headPieces = value[2]
		local s_handle = 'default:stick'
		local s_handlePieces = 0
		local s_scraps = fn_DefineScraps(s_tool)

		recycleage.after_use_overrider(s_tool, s_handle, s_handlePieces,
			s_scraps, i_headPieces)
	end
end


--
-- Main body
--

pr_MTGoverriders()
