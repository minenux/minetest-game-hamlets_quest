--[[

	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Function
--

-- Used to determine which scrap material should be used.
local fn_DefineScraps = function(a_s_itemstring)
	local s_material = ''

	if (string.match(a_s_itemstring, 'mithril') ~= nil) then
		s_Material = 'recycleage:scrap_metal_mithril'

	elseif (string.match(a_s_itemstring, 'silver') ~= nil) then
		s_Material = 'recycleage:scrap_metal_silver'
	end

	return s_material
end


--
-- Procedure
--

-- Used to override the default Minetest Game's tools and weapons.
local pr_moreoresOverriders = function()

	-- Local constants

	-- Default drop amounts per tool's type
	local i_dropPick = minetest.settings:get('recycleage_drop_pick') or 2
	local i_dropShovel = minetest.settings:get('recycleage_drop_shovel') or 0
	local i_dropAxe = minetest.settings:get('recycleage_drop_axe') or 2
	local i_dropSword = minetest.settings:get('recycleage_drop_sword') or 1
	local i_dropHoe = minetest.settings:get('recycleage_drop_hoe') or 1

	t_moreoresTools = {

		-- Mithril
		{"moreores:pick_mithril",	i_dropPick},
		{"moreores:shovel_mithril",	i_dropShovel},
		{"moreores:axe_mithril",	i_dropAxe},
		{"moreores:hoe_mithril",	i_dropHoe},

		-- Silver
		{"moreores:pick_silver",	i_dropPick},
		{"moreores:shovel_silver",	i_dropShovel},
		{"moreores:axe_silver",		i_dropAxe},
		{"moreores:hoe_silver",		i_dropHoe}
	}

	t_moreoresSwords = {
		{"moreores:sword_mithril",	i_dropSword},
		{"moreores:sword_silver",	i_dropSword}
	}


	--
	-- Body
	--

	-- Tools' overrider
	for _, value in pairs(t_moreoresTools) do
		local s_tool = value[1]
		local i_headPieces = value[2]
		local s_handle = 'default:stick'
		local s_handlePieces = 1
		local s_scraps = fn_DefineScraps(s_tool)

		recycleage.after_use_overrider(s_tool, s_handle, s_handlePieces,
			s_scraps, i_headPieces)
	end

	-- Weapons' overrider
	for _, value in pairs(t_moreoresSwords) do
		local s_tool = value[1]
		local i_headPieces = value[2]
		local s_handle = 'default:stick'
		local s_handlePieces = 0
		local s_scraps = fn_DefineScraps(s_tool)

		recycleage.after_use_overrider(s_tool, s_handle, s_handlePieces,
			s_scraps, i_headPieces)
	end
end


--
-- Main body
--

pr_moreoresOverriders()
