-- Localize
local register_hammer = uniham.register_hammer
local S = uniham.translator


register_hammer('hammer_mithril', {
    name = S('Mithril Hammer'),
    head = {
        texture = 'moreores_mithril_block.png',
    },
    craft = 'moreores:mithril_ingot',
    uses = 450
})

register_hammer('hammer_silver', {
    name = S('Silver Hammer'),
    head = {
        texture = 'moreores_silver_block.png',
    },
    craft = 'moreores:silver_ingot',
    uses = 250
})
