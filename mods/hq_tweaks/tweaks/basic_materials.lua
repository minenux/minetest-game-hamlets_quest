--[[

	Basic Materials' tweaks
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Constants
--

local t_ITEMS = {
	'basic_materials:cement_block',
	'basic_materials:padlock',
	'basic_materials:steel_bar',
	'basic_materials:gear_steel',
	'basic_materials:wet_cement',
	'basic_materials:brass_block',
	'basic_materials:brass_ingot',
	'basic_materials:steel_strip',
	'basic_materials:copper_strip',
	'basic_materials:motor',
	--'basic_materials:silicon',
	'basic_materials:plastic_sheet',
	'basic_materials:concrete_block',
	'basic_materials:plastic_strip',
	'basic_materials:heating_element',
	'basic_materials:empty_spool',
	'basic_materials:chainlink_brass',
	'basic_materials:chainlink_steel',
	'basic_materials:gold_wire',
	'basic_materials:steel_wire',
	'basic_materials:copper_wire',
	'basic_materials:paraffin',
	'basic_materials:silver_wire',
	'basic_materials:energy_crystal_simple',
	'basic_materials:chain_brass',
	'basic_materials:chain_steel',
	'basic_materials:terracotta_base',
	'basic_materials:ic',
	'basic_materials:oil_extract'
}


--
-- Procedures
--

-- Unregister crafting recipes
for i_Item = 1, #t_ITEMS do
	minetest.clear_craft({output = t_ITEMS[i_Item]})
	--print('Cleared recipe for: ' .. t_ITEMS[i_Item])
end

-- Unregister items themselves
for i_Item = 1, #t_ITEMS do
	minetest.unregister_item(t_ITEMS[i_Item])
	--print('Item unregistered: ' .. t_ITEMS[i_Item])
end

minetest.unregister_item('basic_materials:silicon')

-- Clear the table for memory saving
t_ITEMS = nil
