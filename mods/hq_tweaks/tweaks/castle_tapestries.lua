--[[

	Castle Tapestries' tweaks
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--- Castle Tapestries 'full node bug' workaround

-- Set tapestries to phantom
local t_Tapestries = {
	'castle_tapestries:tapestry',
	'castle_tapestries:tapestry_long',
	'castle_tapestries:tapestry_very_long'
}

for i_Element = 1, 3 do
	minetest.override_item(t_Tapestries[i_Element], {walkable = false})
end

t_Tapestries = nil
