--[[

	Farming Redo's tweaks
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Change the bowl's name to make it clear that it's for food only.
minetest.override_item("farming:bowl",
	{description = "Wooden Bowl (food only)"})

-- Make the crafting recipe of Jack 'O Lantern more expensive, given that it
-- is an everlasting light source.

minetest.clear_craft({output = 'farming:jackolantern'})

minetest.register_craft({
	output = 'farming:jackolantern',
	recipe = {
		{'default:mese_crystal_fragment'},
		{'group:food_pumpkin'},
	}
})


-- Aliases for backward compatibility

minetest.register_alias('crops:corn', 'farming:corn')
minetest.register_alias('crops:melon', 'farming:melon_8')
minetest.register_alias('crops:potato', 'farming:potato')

minetest.register_alias('crops:tomato', 'farming:tomato')
minetest.register_alias('crops:pumpkin', 'farming:pumpkin_8')
minetest.register_alias('crops:corn_cob', 'farming:corn_cob')
minetest.register_alias('crops:beanpoles', 'farming:beanpole')
minetest.register_alias('crops:clay_bowl', 'farming:bowl')

minetest.register_alias('crops:green_bean', 'farming:beans')
minetest.register_alias('crops:hydrometer', 'farming:hoe_steel')
minetest.register_alias('crops:melon_seed', 'farming:melon_slice')
minetest.register_alias('crops:potato_eyes', 'farming:potato')
minetest.register_alias('crops:tomato_seed', 'farming:tomato')

minetest.register_alias('crops:pumpkin_seed', 'farming:pumpkin_slice')
minetest.register_alias('crops:watering_can', 'farming:hoe_steel')
minetest.register_alias('crops:vegetable_stew', 'farming:pea_soup')
minetest.register_alias('crops:corn_on_the_cob', 'farming:corn_cob')
minetest.register_alias('crops:green_bean_seed', 'farming:beans')

minetest.register_alias('crops:roasted_pumpkin', 'farming:pumpkin_bread')
minetest.register_alias('crops:unbaked_clay_bowl', 'farming:bowl')
minetest.register_alias('crops:soil_with_potatoes', 'farming:soil')
minetest.register_alias('crops:uncooked_vegetable_stew', 'farming:potato_salad')

minetest.register_alias('crops:beanpole_base', 'farming:beanpole')
minetest.register_alias('crops:beanpole_top', 'farming:beanpole')

minetest.register_alias('crops:beanpole_plant_base_1', 'farming:beanpole_1')
minetest.register_alias('crops:beanpole_plant_base_2', 'farming:beanpole_2')
minetest.register_alias('crops:beanpole_plant_base_3', 'farming:beanpole_3')
minetest.register_alias('crops:beanpole_plant_base_4', 'farming:beanpole_4')
minetest.register_alias('crops:beanpole_plant_base_5', 'farming:beanpole_5')
minetest.register_alias('crops:beanpole_plant_base_6', 'farming:beanpole_5')
minetest.register_alias('crops:beanpole_plant_top_1', 'farming:beanpole_1')
minetest.register_alias('crops:beanpole_plant_top_2', 'farming:beanpole_2')
minetest.register_alias('crops:beanpole_plant_top_3', 'farming:beanpole_3')
minetest.register_alias('crops:beanpole_plant_top_4', 'farming:beanpole_4')

minetest.register_alias('crops:pumpkin_plant_1', 'farming:pumpkin_1')
minetest.register_alias('crops:pumpkin_plant_2', 'farming:pumpkin_2')
minetest.register_alias('crops:pumpkin_plant_3', 'farming:pumpkin_3')
minetest.register_alias('crops:pumpkin_plant_4', 'farming:pumpkin_4')
minetest.register_alias('crops:pumpkin_plant_5', 'farming:pumpkin_5')
minetest.register_alias('crops:pumpkin_plant_6', 'farming:pumpkin_6')

minetest.register_alias('crops:pumpkin_plant_5_attached', 'farming:pumpkin_7')

minetest.register_alias('crops:tomato_plant_1', 'farming:tomato_1')
minetest.register_alias('crops:tomato_plant_2', 'farming:tomato_2')
minetest.register_alias('crops:tomato_plant_3', 'farming:tomato_3')
minetest.register_alias('crops:tomato_plant_4', 'farming:tomato_4')
minetest.register_alias('crops:tomato_plant_5', 'farming:tomato_5')
minetest.register_alias('crops:tomato_plant_6', 'farming:tomato_6')

minetest.register_alias('crops:melon_plant_1', 'farming:melon_1')
minetest.register_alias('crops:melon_plant_2', 'farming:melon_2')
minetest.register_alias('crops:melon_plant_3', 'farming:melon_3')
minetest.register_alias('crops:melon_plant_4', 'farming:melon_4')
minetest.register_alias('crops:melon_plant_5', 'farming:melon_5')
minetest.register_alias('crops:melon_plant_6', 'farming:melon_6')
minetest.register_alias('crops:melon_plant_5_attached', 'farming:melon_8')

minetest.register_alias('crops:corn_base_seed', 'farming:corn_1')

minetest.register_alias('crops:corn_base_1', 'farming:corn_1')
minetest.register_alias('crops:corn_base_2', 'farming:corn_2')
minetest.register_alias('crops:corn_base_3', 'farming:corn_3')

minetest.register_alias('crops:corn_top_1', 'farming:corn_4')
minetest.register_alias('crops:corn_top_2', 'farming:corn_5')
minetest.register_alias('crops:corn_top_3', 'farming:corn_6')
minetest.register_alias('crops:corn_top_4', 'farming:corn_7')

minetest.register_alias('crops:potato_plant_1', 'farming:potato_1')
minetest.register_alias('crops:potato_plant_2', 'farming:potato_2')
minetest.register_alias('crops:potato_plant_3', 'farming:potato_3')
minetest.register_alias('crops:potato_plant_4', 'farming:potato_4')
minetest.register_alias('crops:potato_plant_5', 'farming:potato_5')
