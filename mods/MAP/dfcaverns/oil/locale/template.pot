# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-26 00:49-0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: dfcaverns\oil\init.lua:8
msgid ""
"Liquid hydrocarbons formed from the detritus of long dead plants and animals "
"processed by heat and pressure deep within the earth."
msgstr ""

#: dfcaverns\oil\init.lua:9
msgid "Buckets of oil can be used as fuel."
msgstr ""

#: dfcaverns\oil\init.lua:15
msgid "Oil"
msgstr ""

#: dfcaverns\oil\init.lua:65
msgid "Flowing Oil"
msgstr ""

#: dfcaverns\oil\init.lua:194
msgid "Oil Bucket"
msgstr ""
