# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-11 03:51-0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: df_farming\cave_wheat.lua:10
#: df_farming\cave_wheat.lua:87
msgid "Cave Wheat"
msgstr ""

#: df_farming\cave_wheat.lua:79
msgid "Cave Wheat Seed"
msgstr ""

#: df_farming\cave_wheat.lua:100
msgid "Cave Wheat Flour"
msgstr ""

#: df_farming\cave_wheat.lua:108
msgid "Dwarven Bread"
msgstr ""

#: df_farming\cooking.lua:72
msgid "Cave Wheat Flour Biscuit"
msgstr ""

#: df_farming\cooking.lua:73
msgid "Cave Wheat Flour Bun"
msgstr ""

#: df_farming\cooking.lua:74
msgid "Cave Wheat Flour Pancake"
msgstr ""

#: df_farming\cooking.lua:77
msgid "Cave Wheat Seed Loaf"
msgstr ""

#: df_farming\cooking.lua:78
msgid "Cave Wheat Seed Puffs"
msgstr ""

#: df_farming\cooking.lua:79
msgid "Cave Wheat Seed Risotto"
msgstr ""

#: df_farming\cooking.lua:82
msgid "Sweet Pod Spore Dumplings"
msgstr ""

#: df_farming\cooking.lua:83
msgid "Sweet Pod Spore Single Crust Pie"
msgstr ""

#: df_farming\cooking.lua:84
msgid "Sweet Pod Spore Brule"
msgstr ""

#: df_farming\cooking.lua:87
msgid "Sweet Pod Sugar Cookie"
msgstr ""

#: df_farming\cooking.lua:88
msgid "Sweet Pod Sugar Gingerbread"
msgstr ""

#: df_farming\cooking.lua:89
msgid "Sweet Pod Sugar Roll"
msgstr ""

#: df_farming\cooking.lua:92
msgid "Plump Helmet Mince"
msgstr ""

#: df_farming\cooking.lua:93
msgid "Plump Helmet Stalk Sausage"
msgstr ""

#: df_farming\cooking.lua:94
msgid "Plump Helmet Roast"
msgstr ""

#: df_farming\cooking.lua:97
msgid "Plump Helmet Spawn Soup"
msgstr ""

#: df_farming\cooking.lua:98
msgid "Plump Helmet Spawn Jambalaya"
msgstr ""

#: df_farming\cooking.lua:99
msgid "Plump Helmet Sprout Stew"
msgstr ""

#: df_farming\cooking.lua:102
msgid "Quarry Bush Leaf Spicy Bun"
msgstr ""

#: df_farming\cooking.lua:103
msgid "Quarry Bush Leaf Croissant"
msgstr ""

#: df_farming\cooking.lua:104
msgid "Stuffed Quarry Bush Leaf"
msgstr ""

#: df_farming\cooking.lua:107
msgid "Rock Nut Bread"
msgstr ""

#: df_farming\cooking.lua:108
msgid "Rock Nut Cookie"
msgstr ""

#: df_farming\cooking.lua:109
msgid "Rock Nut Cake"
msgstr ""

#: df_farming\cooking.lua:112
msgid "Dimple Cup Spore Flatbread"
msgstr ""

#: df_farming\cooking.lua:113
msgid "Dimple Cup Spore Scone"
msgstr ""

#: df_farming\cooking.lua:114
msgid "Dimple Cup Spore Roll"
msgstr ""

#: df_farming\cooking.lua:117
msgid "Pig Tail Spore Sandwich"
msgstr ""

#: df_farming\cooking.lua:118
msgid "Pig Tail Spore Tofu"
msgstr ""

#: df_farming\cooking.lua:119
msgid "Pig Tail Spore Casserole"
msgstr ""

#: df_farming\cooking.lua:122
msgid "Dwarven Syrup Taffy"
msgstr ""

#: df_farming\cooking.lua:123
msgid "Dwarven Syrup Jellies"
msgstr ""

#: df_farming\cooking.lua:124
msgid "Dwarven Syrup Delight"
msgstr ""

#: df_farming\dimple_cup.lua:10
msgid "Dimple Cup"
msgstr ""

#: df_farming\dimple_cup.lua:68
msgid "Dimple Cup Spores"
msgstr ""

#: df_farming\doc.lua:11
msgid ""
"A meal made from the admixture of two ingredients, it keeps well but are not "
"a rich source of nutrients."
msgstr ""

#: df_farming\doc.lua:13
msgid ""
"A meal made from three ingredients mixed together. They're more wholesome, "
"packing more nutrition into a single serving."
msgstr ""

#: df_farming\doc.lua:15
msgid "Four finely minced ingredients combine into a fine, full meal."
msgstr ""

#: df_farming\doc.lua:21
msgid "Whatever this fungus was in life, it is now dead."
msgstr ""

#: df_farming\doc.lua:22
msgid ""
"Dead fungus quickly decays into an unrecognizable mess. It can be used as "
"weak fuel or terrible decor."
msgstr ""

#: df_farming\doc.lua:24
msgid ""
"A species of lavender mushroom ubiquitous in caves that is most notable for "
"the soft bioluminescence it produces."
msgstr ""

#: df_farming\doc.lua:25
msgid ""
"This mushroom is inedible but continues producing modest levels of light "
"long after it's picked."
msgstr ""

#: df_farming\doc.lua:27
msgid ""
"Cave wheat is literally a breed of grain-producing grass that somehow lost "
"its ability to photosynthesize and adapted to a more fungal style of life."
msgstr ""

#: df_farming\doc.lua:28
msgid ""
"Like its surface cousin, cave wheat produces grain that can be ground into a "
"form of flour."
msgstr ""

#: df_farming\doc.lua:29
msgid "Cave wheat seed ground into a powder suitable for cooking."
msgstr ""

#: df_farming\doc.lua:30
msgid ""
"When baked alone it forms an edible bread, but it combines well with other "
"more flavorful ingredients."
msgstr ""

#: df_farming\doc.lua:31
msgid ""
"Bread baked from cave wheat flour is tough and durable. A useful ration for "
"long expeditions."
msgstr ""

#: df_farming\doc.lua:32
msgid "It's not tasty, but it keeps you going."
msgstr ""

#: df_farming\doc.lua:34
msgid ""
"The distinctive midnight-blue caps of these mushrooms are inverted, exposing "
"their gills to any breeze that might pass, and have dimpled edges that give "
"them their name."
msgstr ""

#: df_farming\doc.lua:35
msgid ""
"Dimple cups can be dried, ground, and processed to extract a deep blue dye."
msgstr ""

#: df_farming\doc.lua:37
msgid ""
"Pig tails are a fibrous fungal growth that's most notable for its twisting "
"stalks. In a mature stand of pig tails the helical stalks intertwine into a "
"dense mesh."
msgstr ""

#: df_farming\doc.lua:38
msgid "Pig tail stalks can be processed to extract fibers useful as thread."
msgstr ""

#: df_farming\doc.lua:39
msgid "Threads of pig tail fiber."
msgstr ""

#: df_farming\doc.lua:40
msgid ""
"A crafting item that can be woven into textiles and other similar items."
msgstr ""

#: df_farming\doc.lua:42
msgid ""
"Plump helmets are a thick, fleshy mushroom that's edible picked straight "
"from the ground. They form a staple diet for both lost cave explorers and "
"the fauna that preys on them."
msgstr ""

#: df_farming\doc.lua:43
msgid ""
"While they can be eaten fresh, they can be monotonous fare and are perhaps "
"better appreciated as part of a more complex prepared dish."
msgstr ""

#: df_farming\doc.lua:45
msgid ""
"A rare breed of fungus from deep underground that produces a bushy cluster "
"of rumpled gray 'blades'. The biological function of these blades is not "
"known, as quarry bushes reproduce via hard-shelled nodules that grow down at "
"the blade's base."
msgstr ""

#: df_farming\doc.lua:46
msgid ""
"Quarry bush leaves and nodules (called 'rock nuts') can be harvested and are "
"edible with processing."
msgstr ""

#: df_farming\doc.lua:47
msgid ""
"The dried blades of a quarry bush add a welcome zing to recipes containing "
"otherwise-bland subterranean foodstuffs, but they're too spicy to be eaten "
"on their own."
msgstr ""

#: df_farming\doc.lua:48
msgid "Quarry bush leaves can be used as an ingredient in foodstuffs."
msgstr ""

#: df_farming\doc.lua:50
msgid ""
"Sweet pods grow in rich soil, and once they reach maturity they draw that "
"supply of nutrients up to concentrate it in their fruiting bodies. They turn "
"bright red when ripe and can be processed in a variety of ways to extract "
"the sugars they contain."
msgstr ""

#: df_farming\doc.lua:53
msgid "When milled, sweet pods produce a granular sugary substance."
msgstr ""

#: df_farming\doc.lua:55
msgid "When dried in an oven, sweet pods produce a granular sugary substance."
msgstr ""

#: df_farming\doc.lua:57
msgid "Crushing them in a bucket squeezes out a flavorful syrup."
msgstr ""

#: df_farming\doc.lua:59
msgid "Sweet pod sugar has a pink tint to it."
msgstr ""

#: df_farming\doc.lua:60
msgid ""
"Too sweet to be eaten directly, it makes an excellent ingredient in food "
"recipes."
msgstr ""

#: df_farming\doc.lua:61
msgid "Sweet pod syrup is thick and flavorful."
msgstr ""

#: df_farming\doc.lua:62
msgid ""
"Too strong and thick to drink straight, sweet pod syrup is useful in food "
"recipes."
msgstr ""

#: df_farming\pig_tail.lua:10
msgid "Pig Tail"
msgstr ""

#: df_farming\pig_tail.lua:78
msgid "Pig Tail Spore"
msgstr ""

#: df_farming\pig_tail.lua:86
msgid "Pig tail thread"
msgstr ""

#: df_farming\plants.lua:10
msgid "Dead Fungus"
msgstr ""

#: df_farming\plants.lua:42
msgid "Cavern Fungi"
msgstr ""

#: df_farming\plump_helmet.lua:61
msgid "Plump Helmet Spawn"
msgstr ""

#: df_farming\plump_helmet.lua:92
#: df_farming\plump_helmet.lua:129
#: df_farming\plump_helmet.lua:164
#: df_farming\plump_helmet.lua:199
#: df_farming\plump_helmet.lua:251
msgid "Plump Helmet"
msgstr ""

#: df_farming\quarry_bush.lua:10
msgid "Quarry Bush"
msgstr ""

#: df_farming\quarry_bush.lua:75
msgid "Rock Nuts"
msgstr ""

#: df_farming\quarry_bush.lua:84
msgid "Quarry Bush Leaves"
msgstr ""

#: df_farming\sweet_pod.lua:10
msgid "Sweet Pod"
msgstr ""

#: df_farming\sweet_pod.lua:74
msgid "Sweet Pod Spores"
msgstr ""

#: df_farming\sweet_pod.lua:82
msgid "Sweet Pods"
msgstr ""

#: df_farming\sweet_pod.lua:98
msgid "Sweet Pod Sugar"
msgstr ""

#: df_farming\sweet_pod.lua:138
msgid "Dwarven Syrup Source"
msgstr ""

#: df_farming\sweet_pod.lua:186
msgid "Flowing Dwarven Syrup"
msgstr ""

#: df_farming\sweet_pod.lua:239
msgid "Dwarven Syrup Bucket"
msgstr ""
