--[[
	Hard Trees Redo - Prevents digging trees by punching them.
	Copyright © 2018, 2019 Hamlet <hamlatmesehub@riseup.net> and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Rarity constants
--

local AVERAGE	= 6.665	-- drop chance: 15%
local SCARCE	= 10	-- drop chance: 10%
local RARE		= 20	-- drop chance: 5%


--
-- Trees groups' constants
--

-- Applies to acacia, jungle tree, tree
local group_nodes_a = {
   groups = {
      tree = 1, choppy = 2, flammable = 2
   }
}

if minetest.get_modpath("fallen_trees") then
   group_nodes_a = {
      groups = {
	 tree = 1, choppy = 2, flammable = 2, falling_node = 1
      }
   }
end

-- Applies to aspen and pine
local group_nodes_b = {
   groups = {
      tree = 1, choppy = 3, flammable = 3
   }
}

if minetest.get_modpath("fallen_trees") then
   group_nodes_b = {
      groups = {
	 tree = 1, choppy = 3, flammable = 3, falling_node = 1
      }
   }
end


--
-- Nodes to be overriden
--

local tree_nodes_a = {
   "real_trees:large_acacia_tree", "real_trees:large_jungle_tree",
   "real_trees:large_tree", "real_trees:medium_acacia_tree",
   "real_trees:medium_jungle_tree", "real_trees:medium_tree",
   "real_trees:small_acacia_tree", "real_trees:small_jungle_tree",
   "real_trees:small_tree", "real_trees:corner_acacia_tree",
   "real_trees:t_corner_acacia_tree", "real_trees:h_large_acacia_tree",
   "real_trees:a_small_acacia_tree", "real_trees:a_medium_acacia_tree",
   "real_trees:a_large_acacia_tree", "real_trees:a_small_jungle_tree",
   "real_trees:a_medium_jungle_tree", "real_trees:a_large_jungle_tree",
   "real_trees:a_small_tree", "real_trees:a_medium_tree",
   "real_trees:a_large_tree"
}

local tree_nodes_b = {
   "real_trees:large_aspen_tree", "real_trees:large_pine_tree",
   "real_trees:medium_aspen_tree", "real_trees:medium_pine_tree",
   "real_trees:small_aspen_tree", "real_trees:small_pine_tree",
   "real_trees:a_small_pine_tree", "real_trees:a_medium_pine_tree",
   "real_trees:a_large_pine_tree", "real_trees:a_small_aspen_tree",
   "real_trees:a_medium_aspen_tree", "real_trees:a_large_aspen_tree"
}


--
-- Wooden nodes groups' overriders
--

for n = 1, 21 do
   minetest.override_item(tree_nodes_a[n], group_nodes_a)
end

for n = 1, 12 do
   minetest.override_item(tree_nodes_a[n], group_nodes_b)
end


--
-- Leaves nodes drops' overriders
--

minetest.override_item("real_trees:acacia_leaf_slab", {
   drop = {
      max_items = 2,
	 items = {
	    {items = {"default:acacia_sapling"}, rarity = 40},
	    {items = {"real_trees:acacia_leaf_slab"}},
	    {items = {"default:stick"}, rarity = RARE}
	 }
   }
})

minetest.override_item("real_trees:aspen_leaf_slab", {
   drop = {
      max_items = 2,
	 items = {
	    {items = {"default:aspen_sapling"}, rarity = 40},
	    {items = {"real_trees:aspen_leaf_slab"}},
	    {items = {"default:stick"}, rarity = SCARCE}
	 }
   }
})

minetest.override_item("real_trees:jungle_leaf_slab", {
   drop = {
      max_items = 2,
	 items = {
	    {items = {"default:junglesapling"}, rarity = 40},
	    {items = {"real_trees:jungle_leaf_slab"}},
	    {items = {"default:stick"}, rarity = AVERAGE}
	 }
   }
})

minetest.override_item("real_trees:pine_needle_slab", {
   drop = {
      max_items = 2,
	 items = {
	    {items = {"default:pine_sapling"}, rarity = 40},
	    {items = {"real_trees:pine_needle_slab"}},
	    {items = {"default:stick"}, rarity = RARE}
	 }
   }
})


minetest.override_item("real_trees:leaf_slab", {
   drop = {
      max_items = 2,
	 items = {
	    {items = {"default:sapling"}, rarity = 40},
	    {items = {"real_trees:leaf_slab"}},
	    {items = {"default:stick"}, rarity = SCARCE}
	 }
   }
})


--
-- Flush the no longer used constants and tables
--

AVERAGE = nil
SCARCE = nil
RARE = nil
group_nodes_a = nil
group_nodes_b = nil
tree_nodes_a = nil
tree_nodes_b = nil
