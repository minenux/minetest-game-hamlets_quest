--[[
    Fallen Trees - Helps preventing the "floating treetops effect".
	Copyright © 2018, 2019 Hamlet <hamlatmesehub@riseup.net> and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Global mod's namespace
--

fallen_trees = {}


--
-- Tree nodes' table
--

local tree_nodes_list = {}


--
-- Functions
--

-- Function to inject the new group into the existing groups table.
fallen_trees.overrider = function(nodestring)
	local registered_nodes = minetest.registered_nodes

	for _, element in pairs(registered_nodes) do

		-- Check if the node actually exists.
		if (element.name == nodestring) then
			local old_groups = element.groups
			local new_group = {falling_node = 1}

			--[[
				The following weird code is due the fact that
				Lua 5.1 does not allow to simply join two tables
				by doing, e.g. table_3 = (table_1 + table_2).
			--]]

			-- This converts the tables into strings
			new_group = minetest.serialize(new_group)
			old_groups = minetest.serialize(old_groups)

			-- This joins the two "string tables"
			local new_groups = new_group .. old_groups
			--print("Joined groups: " .. new_groups)

			-- This replaces the "junction mark" that would prevent
			-- using the string with the deserialize function.
			new_groups = string.gsub(new_groups, "}return {", ", ")

			-- This converts the joined "string table"
			-- into an actual table.
			new_groups = minetest.deserialize(new_groups)

			--print("New groups: " .. dump(new_groups))

			-- The groups table must have this format.
			new_groups = {
				groups = new_groups
			}

			minetest.override_item(nodestring, new_groups)
		end
	end
end


-- Function to scan for the registered tree nodes and populate their
-- table.
local trees_scanner = function()
	local registered_nodes = minetest.registered_nodes

	for _, element in pairs(registered_nodes) do
		if (minetest.get_item_group(element.name, "tree") ~= 0) then
			local already_listed = false

			--print("Tree node found: " .. element.name)

			for index = 1, #tree_nodes_list do
				if (tree_nodes_list[index] == element.name) then
					already_listed = true
				end
			end

			if (already_listed == false) then
				table.insert(tree_nodes_list, element.name)
			end
		end
	end
end


--
-- Execute and override.
--

-- Populate the tree nodes' list.
trees_scanner()

--print(dump(tree_nodes_list))

-- Override the trees nodes' groups
for element = 1, #tree_nodes_list do
	--print("Overriding: " .. tree_nodes_list[element])
	fallen_trees.overrider(tree_nodes_list[element])
end


--
-- Flush the table for memory saving.
--

tree_nodes_list = nil


--
-- Minetest engine debug logging
--

if (minetest.settings:get("debug_log_level") == nil)
or (minetest.settings:get("debug_log_level") == "action")
or (minetest.settings:get("debug_log_level") == "info")
or (minetest.settings:get("debug_log_level") == "verbose")
then
	minetest.log("action", "[Mod] Fallen Trees [v1.3.0] loaded.")
end
