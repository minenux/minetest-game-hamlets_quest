--[[
	Fallen Nodes - Adds dirt, cobble, snow, straw and cactus nodes to
	the	falling_node group. Papyrus will fall too.
	Copyright © 2018, 2019 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Global mod's namespace
--

fallen_nodes = {}


--
-- Variables
--

local b_DisableSnowBlocks = minetest.settings:get_bool('fallen_nodes_snow')
if (b_DisableSnowBlocks == nil) then
	b_DisableSnowBlocks = false
end

local s_LogLevel = minetest.settings:get('debug_log_level')
local s_ModPath = minetest.get_modpath('fallen_nodes')

-- Minetest Game v5.1.0's nodes to be overriden.
local t_NodesList = {}

if (b_DisableSnowBlocks == false) then
	t_NodesList = {
		-- Dirt nodes
		'default:dirt',
		'default:dirt_with_coniferous_litter',
		'default:dirt_with_dry_grass',
		'default:dirt_with_grass',
		'default:dirt_with_grass_footsteps',
		'default:dirt_with_rainforest_litter',
		'default:dirt_with_snow',
		'default:dry_dirt',
		'default:dry_dirt_with_dry_grass',

		-- Cobble nodes
		'default:cobble',
		'default:desert_cobble',
		'default:mossycobble',
		'stairs:slab_cobble',
		'stairs:slab_desert_cobble',
		'stairs:stair_cobble',
		'stairs:stair_desert_cobble',
		'stairs:stair_inner_cobble',
		'stairs:stair_inner_desert_cobble',
		'stairs:stair_inner_mossycobble',
		'stairs:stair_mossycobble',
		'stairs:stair_outer_cobble',
		'stairs:stair_outer_desert_cobble',
		'stairs:stair_outer_mossycobble',
		'walls:cobble',
		'walls:mossycobble',
		'walls:desertcobble',

		-- Snow nodes
		'default:snowblock',
		'stairs:slab_snowblock',
		'stairs:stair_inner_snowblock',
		'stairs:stair_outer_snowblock',
		'stairs:stair_snowblock',

		-- Straw nodes
		'farming:straw',
		'stairs:slab_straw',
		'stairs:stair_inner_straw',
		'stairs:stair_outer_straw',
		'stairs:stair_straw',

		-- Cactus node
		'default:cactus'
	}

else
	t_NodesList = {
		-- Dirt nodes
		'default:dirt',
		'default:dirt_with_coniferous_litter',
		'default:dirt_with_dry_grass',
		'default:dirt_with_grass',
		'default:dirt_with_grass_footsteps',
		'default:dirt_with_rainforest_litter',
		'default:dirt_with_snow',
		'default:dry_dirt',
		'default:dry_dirt_with_dry_grass',

		-- Cobble nodes
		'default:cobble',
		'default:desert_cobble',
		'default:mossycobble',
		'stairs:slab_cobble',
		'stairs:slab_desert_cobble',
		'stairs:stair_cobble',
		'stairs:stair_desert_cobble',
		'stairs:stair_inner_cobble',
		'stairs:stair_inner_desert_cobble',
		'stairs:stair_inner_mossycobble',
		'stairs:stair_mossycobble',
		'stairs:stair_outer_cobble',
		'stairs:stair_outer_desert_cobble',
		'stairs:stair_outer_mossycobble',
		'walls:cobble',
		'walls:mossycobble',
		'walls:desertcobble',

		-- Straw nodes
		'farming:straw',
		'stairs:slab_straw',
		'stairs:stair_inner_straw',
		'stairs:stair_outer_straw',
		'stairs:stair_straw',

		-- Cactus node
		'default:cactus'
	}
end


--
-- Function
--

--[[
	CREDITS:
	This function has been based on TenPlus1's code posted here
	https://forum.minetest.net/viewtopic.php?p=361200#p361200
	which allowed to drastically reduce the code's length, as well as
	preventing potential bugs.
--]]

fallen_nodes.TurnToFalling = function(a_node_string)

	-- If the node has been registered
	if minetest.registered_nodes[a_node_string] then
		--print('Overriding: ' .. a_node_string)

		local t_FallingGroup = {falling_node = 1}

		-- Get the actual groups table
		local t_Groups = minetest.registered_nodes[a_node_string].groups

		-- Convert it to a string
		local s_Groups = minetest.serialize(t_Groups)
		--print('Old groups table = ' .. dump(t_Groups))

		-- Insert the new group
		t_Groups[(#t_Groups + 1)] = t_FallingGroup
		s_Groups = minetest.serialize(t_Groups)

		-- Remove the unnecessary curly brackets
		local s_Groups2 = string.gsub(s_Groups, '{{', '{')
		local s_Groups3 = string.gsub(s_Groups2, '},', ',')

		-- Convert the string to a table
		t_Groups = minetest.deserialize(s_Groups3)
		--print('New groups table: ' .. dump(t_Groups))

		-- Override the node
		minetest.override_item(a_node_string, {groups = t_Groups})
	end
end


--
-- Overriders
--

for i_Element = 1, #t_NodesList do
	fallen_nodes.TurnToFalling(t_NodesList[i_Element])
end

--[[
minetest.override_item('default:papyrus', {
	groups = {
		snappy = 3, flammable = 2, attached_node = 1
	}
})
--]]

--
-- Support for other modules
--

if (minetest.get_modpath('darkage') ~= nil) then
	dofile(s_ModPath .. '/darkage.lua')
end

if (minetest.get_modpath('landscape') ~= nil) then
	fallen_nodes.TurnToFalling('landscape:full_grass_block')
end

if (minetest.get_modpath('mg') ~= nil) then
	fallen_nodes.TurnToFalling('mg:dirt_with_dry_grass')
end


--
-- Variables flushing for memory saving
--

b_DisableSnowBlocks = nil
s_ModPath = nil
t_NodesList = nil


--
-- Minetest engine debug logging
--

if (s_LogLevel == nil)
or (s_LogLevel == 'action')
or (s_LogLevel == 'info')
or (s_LogLevel == 'verbose')
then
	s_LogLevel = nil
	minetest.log('action', '[Mod] Fallen Nodes [v1.5.0] loaded.')
end
